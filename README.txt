This web design is tested on 1080p native resolution / 100% scaling

Sources:

Homepage
https://i.pinimg.com/originals/68/20/a0/6820a03706cf84ea9c448d6cbc8a0efb.jpg
https://blog.tubikstudio.com/case-study-tasty-burger-ui-design-for-food-ordering-app/

About Us
https://images.all-free-download.com/images/graphiclarge/burger_store_facade_design_with_food_on_window_6825108.jpg

Images for cards
https://www.flaticon.com/premium-icon/store_869687?term=store&page=1&position=1&page=1&position=1&related_id=869687&origin=search
https://www.flaticon.com/free-icon/rating_4149881?term=customers&page=1&position=9&page=1&position=9&related_id=4149881&origin=search
https://www.flaticon.com/free-icon/hamburger_3075977?term=burgers&page=1&position=1&page=1&position=1&related_id=3075977&origin=search

Coming Soon Video
https://www.youtube.com/watch?v=dA0VGEbbw4g (Downloaded Locally)

SVG Folder contents obtained from https://icons8.com/

Menu
https://vokasi.co.id/wp-content/uploads/2020/03/burger-beef.jpg
https://www.unileverfoodsolutions.lk/dam/global-ufs/mcos/meps/sri-lanka/calcmenu/recipes/LK-recipes/general/crispy-fried-chicken-burger/main-header.jpg
https://truffle-assets.imgix.net/20089644-landscape.jpg
https://www.unileverfoodsolutions.com.my/dam/global-ufs/mcos/SEA/calcmenu/recipes/MY-recipes/appetisers/kentang-goreng-pasti-sedap/main-header.jpg
https://www.koreanbapsang.com/wp-content/uploads/2020/07/DSC1848.jpg
https://goldbelly.imgix.net//uploads/showcase_media_asset/image/85530/ny-sour-pickles-1-gallon.ffc0f44a0be85000ee4399f282cd751c.jpg
https://cdn-2.tstatic.net/tribunnews/foto/bank/images/ilustrasi-minuman-soft-drink.jpg
https://www.thespruceeats.com/thmb/6VtnTRVYoZuVPugf0D6dH025oqs=/1333x1000/smart/filters:no_upscale()/air-fryer-onion-rings-4771524-12-7434d703a21b47d28ebc88bca3c55498.jpg
https://i.ytimg.com/vi/-f3xtWvOopg/maxresdefault.jpg